---
title: "Instalar Gitea en Debian"
date: 2018-12-10T23:37:28+01:00
author: "Tarteka"
tags: 
  - Gitea
  - Debian
  - Tutorial
---

No me quiero alargar demasiado y solo voy a redactar lo básico para su instalación en un servidor **Debian con Apache**. Si quieres saber más sobre el proyecto, puedes visitar su página web en [gitea.io](https://gitea.io)

## Primeros pasos

Instalar Git y crear un usuario nuevo (llamado git), sin permisos de root, ni de login.

```bash
sudo apt install git
sudo adduser --disabled-login --gecos 'gitea' git
```

Nos pasamos al usuario git:

```bash
su - git
```

## Instalar Gitea

Descargar Gitea en su versión más actual (y preferiblemente estable) y para tu arquitectura. A la creación de este artículo, la versión más actual es la `1.4.3`.

```bash
wget https://dl.gitea.io/gitea/1.4.3/gitea-1.4.3-linux-amd64 -O gitea
chmod +x gitea
```

Crar carpetas necesarias

```bash
mkdir -p custom/conf
mkdir data
```

Iniciar el instalador

```bash
./gitea web
```

Desde un navegador web entras en `tuIP:3000` (recuerda que tienes que tener el puerto 3000 abierto) y verás un formulario. Aquí tendrás que rellenar lo que se te pida.

Si no tienes el puerto abierto. Lo puedes hacer con `ufw allow 3000`.

## Crear certificado con Certbot (letsencrypt)

```bash
sudo apt-get install python-certbot-apache -t stretch-backports
sudo certbot certonly --authenticator standalone --pre-hook "apachectl -k stop" --post-hook "apachectl -k start"
sudo certbot renew --dry-run
```

## Configurar subdomio con proxy inverso

Este es el archivo que uso yo:

```apache
<VirtualHost *:80>
  ServerName gitea.libretux.com
  ProxyPreserveHost On
  ProxyRequests off
  ProxyPass / http://libretux.com:3000/
  ProxyPassReverse / http//libretux.com:3000/

  RewriteEngine On
  RewriteCond %{HTTPS} off
  RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>

<IfModule mod_ssl.c>
<VirtualHost *:443>
  ServerName gitea.libretux.com
  ServerAdmin webmaster@localhost

	SSLProxyCheckPeerCN off
	SSLProxyCheckPeerExpire off
	SSLProxyCheckPeerName off
	SSLProxyEngine On
	SSLProxyVerify none
	ProxyPreserveHost On
	ProxyPass / http://libretux.com:3000/
	ProxyPassReverse / http://libretux.com:3000/
	RequestHeader set X-Forwarded-Proto https

	SSLCertificateFile /etc/letsencrypt/live/gitea.libretux.com/fullchain.pem
	SSLCertificateKeyFile /etc/letsencrypt/live/gitea.libretux.com/privkey.pem
	Include /etc/letsencrypt/options-ssl-apache.conf

  ErrorLog /var/www/gitea.libretux/log/error.log
  CustomLog /var/www/gite.libretux/log/access.log combined

</VirtualHost>
</IfModule>
```

## Inicio automático de Gitea

Para ello vamos a usar `supervisor`

```bash
sudo apt install supervisor
```

Creamos el archivo de configuración (por ejemplo `gitea.conf`) en `/etc/supervisor/conf.d`

```bash
[program:gitea]
directory=/home/git/
command=/home/git/gitea web
autostart=true
autorestart=true
startsecs=10
stdout_logfile=/var/log/gitea/stdout.log
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
stdout_capture_maxbytes=1MB
stderr_logfile=/var/log/gitea/stderr.log
stderr_logfile_maxbytes=1MB
stderr_logfile_backups=10
stderr_capture_maxbytes=1MB
environment = HOME="/home/git", USER="git"
user = git
```

Iniciamos la instancia:

```bash
supervisorctl start gitea
```