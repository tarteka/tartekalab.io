---
title: "Gestionar tus contraseñas"
date: 2019-03-26
resources:
- name: featuredImage
  src: "pass.jpeg"
  params:
    description: "contraseña"
---

Esta entrada será breve y de guía para cuando vuelva a necesitarlo, tener un acceso rápido a todo lo que necesito. Por lo tanto:

> __Así he configurado mi PC para gestionar y usar mis contraseñas de internet.__

Lo primero de todo es tener instalado y configurado _Password Store_. Para ello puedes seguir el artículo de __Victorhck__ [Password Store el gestor de contraseñas estándar de Unix](https://victorhckinthefreeworld.com/2017/05/30/password-store-el-gestor-de-contrasenas-estandar-de-unix/).

Ahora toca la sincronización con Git. Y para ello volvemos a leernos otro artículo de __Victorhck__: [Cómo sincronizar mediante git nuestras contraseñas de pass](https://victorhckinthefreeworld.com/2018/09/26/como-sincronizar-mediante-git-nuestras-contrasenas-de-pass/)

