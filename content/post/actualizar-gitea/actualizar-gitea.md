---
title: "Actualizar Gitea"
date: 2019-01-14T12:42:44+01:00
featuredImg: ""
tags: 
  - Gitea
  - Debian
  - Tutorial
---

1. Nos pasamos al usuario `git`: `sudo su git`.
2. Paramos las máquinas (en mi caso con): `supervisorctl stop gitea`.
3. Renombramos el archivo `~/git/gitea` a `~/git/gitea-old`.
4. Descargamos la nueva versión adecuada a tu arquitectura `wget https://dl.gitea.io/gitea/1.6.3/gitea-1.6.3-linux-amd64` (esta es la última versión a día de hoy).
5. El archivo descargado lo renombramos a `gitea`.
6. Le damos permisos de ejecución `chmod +x gitea`.
6. Iniciamos máquinas de nuevo: `supervisorctl start gitea`.
7. Si todo ha salido bien, podemos eliminar el archivo `gitea-old`.