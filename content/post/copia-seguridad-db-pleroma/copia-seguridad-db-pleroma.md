---
title: "Copia Seguridad de la Base de Datos de Pleroma"
date: 2019-02-11T12:19:49+01:00
draft: false
toc: false
images:
tags: 
  - Pleroma
  - Debian
---

La idea es automatizar la creación de copias de seguridad de nuestra base de datos de Pleroma. Quiero que todos los días creemos un nuevo archivo de respaldo, y que a su vez, elimine respaldos antiguos para no saturarnos de copias de seguridad.

Para ello vamos a tirar de un script en bash y crontab.

Nuestro script será el siguiente:

```bash
#!/bin/bash
HOY=`date +"%d-%m-%Y_%H%M"`
FILENAME="/var/lib/pleroma/pleroma-db-backup/pleroma_dev-${HOY}.sql"

pg_dump -d pleroma_dev --format=custom -f $FILENAME
find /var/lib/pleroma/pleroma-db-backup/pleroma_dev* -mtime +3 -exec rm {} \;
```

En teoría el código de arriba realiza una copia de seguridad de la base de datos con el formato `pleroma_dev-DD-MM-AAAA_HM.sql`. Después revisa el directorio donde guardo las copias de seguridad y elimina toda aquella que tenga más de 3 días. Esto hará que solo guarde 3 copias.

Ahora toca automatizarlo utilizando `cron`. Para ello abrimos `crontab -e` y añadimos la siguiente regla:

```bash
00 05 * * * /var/lib/pleroma/pleroma-scripts/pl-db-bak.sh
```

Con esto estamos indicando que queremos que todos los días a las 5 de la mañana, el usario `pleroma` ejecute el script de actualización `pl-db-bak.sh`

Así lo hago yo. Un script sencillo ya que no soy ningún experto en estas cosas. A veces pienso que este servidor es una bomba de relojería que en cualquier momento puede explotar y dejar caído para siempre el servidor... ¿Conoceis alguna otra forma?