---
title: "Calendario Con Arte 2019"
date: 2018-12-15T18:00:09+01:00
author: "Tarteka"
resources:
- name: featuredImage
  src: "calendario-melina7.jpeg"
  params:
    description: "Calendario artístico"
---

> En esta vida no todos son ceros y unos, y resulta alentador recordar el mundo analógico en su forma más primitiva que conocemos: la pintura. 

Apenas a unos días de entrar en el nuevo año, toca ir pensando en renovar ese calendario que tenemos en casa colgando. Y por eso, dejad que os muestre un calendario creado por una artista, capaz de transmitir sentimientos en cada trazo de sus creaciones.

**Melina Herrero Vilches, es una Licenciada en Bellas Artes afincada en Valenciada**, que desde pequeña sabía que lo suyo era el arte. Siempre deseosa de transmitir sus pensamientos y sueños a través del dibujo.

![](/img/calendario/calendario-melina0.jpeg)

> Os presento el nuevo calendario 2019 que con mucho cariño he preparado para todos vosotros, con 12 dibujos creados especialmente para la ocasión. Ha sido un proceso delicioso el ir haciendo poco a poco las ilustraciones imaginándome a cada uno de vosotros, mis queridos amigos, girando cada página del calendario una vez al mes y disfrutando de mis dibujos en vuestras casas. Si quieres el calendario escríbeme y muchas gracias por colaborar en que este sueño artístico se haya hecho realidad un año más. Abrazos para todos!!! _Melina Herrero Vilches_

No es fácil ganarse la vida con el trabajo de tus sueños, y como tal, todos los años realiza un calendario de pared con temáticas diferentes. Gracias a los aportes conseguidos, es capaz de financiarse otros proyectos en los que trabaja.

Si os gusta su trabajo, podeis comprar un **calendario por el precio de 13€** la unidad.

No encontrarás una tienda online ni pasarelas de pago. La forma para contactar con Melina es su e-mail:

+ `melinarosa_@hotmail.com`

![](/img/calendario/calendario-melina.jpeg)
![](/img/calendario/calendario-melina3.jpeg)
![](/img/calendario/calendario-melina5.jpeg)
![](/img/calendario/calendario-melina7.jpeg)
![](/img/calendario/calendario-melina8.jpeg)
![](/img/calendario/calendario-melina6.jpeg)