---
title: "Instalar Pleroma en Debian"
date: 2018-12-10T23:35:29+01:00
author: "Tarteka"
tags: 
  - Pleroma
  - Debian
  - Tutorial
---

> Traducción cuasi literal de la [wiki oficial](https://git.pleroma.social/pleroma/pleroma/wikis/Installing%20on%20Debian%20based%20distributions).

_Esta guía asume que tienes Debian 9 (Strecth) con Apache instalado y en funcionamiento._

## Requerimientos de Software

+ PostgreSQL 9.6+ (postgresql-contrib-9.6)
+ Elixir 1.5 or newer (No utilices los repos de Debian) 
+ erlang-dev
+ erlang-tools
+ erlang-parsetools
+ erlang-xmerl
+ git
+ build-essential
+ openssh
+ openssl
+ Apache
+ certbot (para certificados Let's encrypt)

## Preparando el sistema

```bash
sudo apt update && sudo apt upgrade
sudo apt install git build-essential openssl ssh sudo postgresql-9.6 postgresql-contrib-9.6 certbot python-certbot-apache
```

## Instalar Elixir y Erlang

```bash
wget -P /tmp/ https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
sudo dpkg -i /tmp/erlang-solutions_1.0_all.deb
sudo apt update
sudo apt install elixir erlang-dev erlang-parsetools erlang-xmerl erlang-tools
```

## Instalar Pleroma BE

+ Crear usuario con privilegios root, y clonar pleroma:
```bash
adduser pleroma
usermod -aG sudo pleroma
su pleroma
cd ~
git clone https://git.pleroma.social/pleroma/pleroma
cd pleroma
```

+ Instalamos las dependencias para pleroma. Respondemos `yes` si nos pregunta por instalar `Hex`:
```bash
mix deps.get
```

+ Generar el archivo de configuración con --> `mix generate_config`:
	- Respondemos `yes` si nos pregunta por instalar `rebar3`
	- Durante la configuración responderemos a las preguntas que nos hace:
		- Dirección DNS. Ejemplo: pleroma.example.com
		- Nombre de la instancia. Ejemplo: Mi pleroma
		- email del administrador.
	- Al final, tendremos un nuevo archivo `config/generated_config.exs`
+ Renombrar el archivo de configuración para que Pleroma lo pueda usar:
```bash
mv config/{generated_config.exs,prod.secret.exs}
```
+ Anteriormente nos habrá creado un archivo nuevo `config/setub_db.psql`. Con el, crearemos la base de datos:
```bash
sudo su postgres -c 'psql -f config/setup_db.psql'
```
+ Ejecutaremos la migración de la base de datos:
```bash
MIX_ENV=prod mix ecto.migrate
```
+ Podemos iniciar Pleroma
```bash
MIX_ENV=prod mix phx.server
```

## Configurando el servidor

Creamos nuestro certificado SSL:

```bash
sudo certbot certonly --authenticator standalone --pre-hook "apachectl -k stop" --post-hook "apachectl -k start"
sudo certbot renew --dry-run
```

Creamos un vhost nuevo en Apache. Por ejemplo lo puedes llamar `pleroma.conf`.

```bash
nano /etc/apache2/sites-available/pleroma.conf
```

Copiamos el siguiente archivo [pleroma.conf](https://gitea.libretux.com/tarteka/micro-codigos/src/branch/master/pleroma/pleroma.conf)

Habilitar el vhost, y reiniciar apache:

```bash
sudo a2ensite pleroma.conf
sudo systemctl start pleroma
```

## Systemd

Copiar el archivo de ejemplo

```bash
sudo cp /home/pleroma/pleroma/installation/pleroma.service /usr/lib/systemd/system/pleroma.service
```
Cambiar la variable:
```bash
Environment="MIX_ENV=prod"
```
Habilitar e iniciar el servicio:
```bash
sudo systemctl enable --now pleroma.service
```

Ahora si navegamos a la dirección web `pleroma.example.com` deberíamos ver nuestra nueva instancia de pleroma.

## Alguna cosilla post-instalación

Si queremos deshabilitar el registro, editamos el archivo `~/pleroma/config/prod.secret.exs` y cambiamos `registrations_open: true` a `false`.