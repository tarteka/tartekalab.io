---
title: "Audio por defecto en Ubuntu"
date: 2018-12-19T13:46:01+01:00
tags: 
  - Ubuntu
  - Tutorial
---

En mi Ubuntu 18.10 (e incluso en la 18.04), cada vez que reiniciaba el ordenador, me cambiaba la confugiracón de mis entradas y salidas de audio.

Para solicionar este error me he basado en el siguiente artículo: [Setting Default Audio Device in Ubuntu 18.04](https://rastating.github.io/setting-default-audio-device-in-ubuntu-18-04/).

### Obtener listado de dispositivos de audio

Para saber los dispositivos que tenemos en nuestro ordenador, tendremos que usar los siguientes dos comandos:

+ Listar dispositivos de salida: `pactl list short sinks`
+ Listar dispositivos de entrada: `pactl list short sources`

Un ejemplo de salida del comando `pactl list short sinks`:

```
0	alsa_output.pci-0000_01_00.1.hdmi-stereo...
1	alsa_output.usb-Samson_Technologies_Samson_Q2U_Microphone...
2	alsa_output.pci-0000_00_1f.3.analog-stereo...
```
Nos quedamos con el número que nos interese (en mi caso el número 2). Repetimos el proceso con los dispositivos de entrada, y apuntamos el número deseado.

### Definir dispostivos de audio por defecto

Ahora toca hacer permanente los cambios para que al reiniciar no se desconfigure la cosa.

Abrimos el archivo `/etc/pulse/default.pa` y nos movemos hasta el final del archivo y modificamos para que quede así:

```
### Make some devices default
set-default-sink 2
set-default-source 2
```

También habrá que comentar lo siguiente:

```
### Use hot-plugged devices like Bluetooth or USB automatically (LP: #1702794)
#.ifexists module-switch-on-connect.so
#load-module module-switch-on-connect
#.endif
```

Ahora solo nos falta eliminar la carpeta `~/.config/pulse` y reiniciar el ordenador.

Si todo ha salido bien, tanto el audio de entrada como de salida deberián ser los que hemos decidido nosotros.