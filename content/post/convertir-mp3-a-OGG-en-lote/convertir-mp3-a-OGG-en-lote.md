---
title: "Convertir Mp3 a OGG en Lote"
date: 2018-12-10T23:33:25+01:00
author: "Tarteka"
tags: 
  - Tutorial
  - Audio
---

Instalar:

```bash
sudo apt install dir2ogg
```

Ejecutar:

```
dir2ogg -r /carpeta/de/tus/mp3s/
```

## Bola extra:

__Crear una lista m3u de tus canciones__

```
find /ruta/a-tu/musica/ -iname *.ogg > lista.m3u
```