---
title: "Instalar y automatizar un blog Jekyll"
date: 2018-12-10T23:39:12+01:00
author: "Tarteka"
tags: 
  - Tutorial
  - Jekyll
---

Vamos a intentar explicar como configurar e instalar Jekyll en tu propio servidor. Y además, gracias a Git, automatizaremos las actualizaciones del blog desde el mismo servidor, sólo con subir una nueva entrada.

> Antes de empezar. Dejar claro, que para realizar esta entrada, me he basado en un [tutorial][tutorial] de la [comunidad de Digital Ocean][comunidad-DO]. No es una traducción literal, y me tomo la libertad de añadir o modificar cosas.

## Prepara tu ordenador

Lo primero, necesitas tener instalado Ruby, Jekyll y Git en tu ordenador de casa.

Una forma génerica para **instalar Ruby**, puede ser la siguiente:

```terminal
curl -L https://get.rvm.io | bash -s stable --ruby
```

Si por ejemplo, estás usando una distribución como Arch o derivadas, se puede instalar desde sus repositorios:

```terminal
sudo pacman -S ruby
```

Ahora que ya tenemos `Ruby` instalado en nuestro ordenador, procederemos a instalar Jekyll. Para ello vamos a usar la _"gema"_ que tiene Ruby.

```terminal
gem install Jekyll
```


Por último, necesitamos instalar Git. Lo podemos descargar directamente desde su [página web][git]. Los usuarios de mac, pueden usar [Homebrew][homebrew]. Los _"linuxeros"_, si no lo teneís instalado ya, podeís usar vuestro repositorio. En arch:

```terminal
sudo pacman -S git
```


## Creando nuestro blog en Jekyll

> Recuerda que todavía estamos en nuestro ordenador

>El objetivo de esta guía no es profundizar en el uso de Jekyll. Aquí solamente, daremos unas pinceladas. Para más información y documentación, es imprescindible acceder a la [página web de Jekyll][jekyll].

Abrimos nuestra terminal, y creamos un blog Jekyll:

```terminal
jekyll new miblog
```


Esto nos creará una carpeta llamada `miblog`, donde tendremos la estrucura básica de nuestro blog.

```terminal
miblog
├── _config.yml
├── _drafts
|   ├── begin-with-the-crazy-ideas.textile
|   └── on-simplicity-in-technology.markdown
├── _includes
|   ├── footer.html
|   └── header.html
├── _layouts
|   ├── default.html
|   └── post.html
├── _posts
|   ├── 2007-10-29-why-every-programmer-should-play-nethack.textile
|   └── 2009-04-26-barcamp-boston-4-roundup.textile
├── _data
|   └── members.yml
├── _site
├── .jekyll-metadata
└── index.html
```


Después de realizar los cambios necesarios en `_config.yml`, y crear alguna entrada nueva, puedes observar su funcionamiento correcto en local:

```terminal
cd miblog
jekyll serve
```


Ahora desde tu navegador, dirígiete a `http://localhsot:4000`, y fíjate que funciona bien.

Para finalizar en nuestro ordenador, inciaremos un repositorio Git dentro del directorio del blog (en nuestro caso `miblog`)

```terminal
git init
git add .
git commit -m "nuestro primer commit"
```


## Preparando nuestro servidor VPS

Para no alargarnos demasiado, voy a suponer que ya tienes el servidor preparado con Apache o Nginx. Y que tu carpeta pública es `/var/www`.

Primero, __instala Git en el VPS__. En el caso de __Debian__ o __Ubuntu__:

```terminal
apt-get install git-core
```


En el caso de otras distribuciones, el proceso de instalación puede variar. En __Fedora__, por ejemplo,  `yum install git-core`.

Igual que en nuestro ordenador, toca instalar `Ruby` y `Jekyll`.

```terminal
curl -L https://get.rvm.io | bash -s stable --ruby
gem install jekyll
```


> Antes no lo he comentado, pero si la instalación de `Ruby`, nos da problemas, puede ser por no tener la llave:

```terminal
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
```


> y una vez instalado, ejecutaremos `source /etc/profile.d/rvm.sh`

Una vez instalado lo anterior, nos dirigimos a nuestra `/home` y creamos un _"repositorio desnudo (bare repository)"_ para trabajar en el.

```terminal
cd ~/
mkdir repos && cd repos
mkdir miblog.git && cd miblog.git
git init --bare
```


Bien, ya casi estamos terminando.

Ahora tenemos que configurar un `hook (gancho o acción)` llamado `post-receive` (o el nombre que tu quieras darle). Este será un `shell script` de Git, que se ejecutará cuando algún archivo sea subido al repositorio (cuando realicemos un `push`).

```terminal
cd repos/miblog.git/hooks
touch post-receive
nano post-receive
```


Ahora copia el siguiente _script_, ajustando las variables `GIT_REPO` (dirección donde hemos creado el repositorio), `TMP_GIT_CLONE` (donde generaremos el blog antes de copiarlo a `/var/www`) y `PUBLIC_WWW`.

```terminal
#!/bin/bash -l
GIT_REPO=$HOME/repos/miblog.git
TMP_GIT_CLONE=$HOME/tmp/git/miblog
PUBLIC_WWW=/var/www/

git clone $GIT_REPO $TMP_GIT_CLONE
jekyll build --source $TMP_GIT_CLONE --destination $PUBLIC_WWW
rm -Rf $TMP_GIT_CLONE
exit
```


Guardamos `control+O`, salimos `control+X` y le damos permisos de ejecución `chomd +x post-receive`.

## Añadiendo el repositorio remoto en nuestro Git local

Ya solo nos queda __volver a nuestro ordenado de casa__ y añadir la sincronización con nuestro repositorio remoto (el del servidor VPS)

```terminal
git remote add droplet usuario@midominio.org:repos/miblog.git
```


Ahora ya con un simple `git push droplet master` hacemos toda la magía.

## Bonus: Copia de seguridad en Github

Para el caso yo usaré `Github`, pero puedes usar cualquier otro servicio cómo `Bitbukcet`, `Gitlab`, etc...

Creamos un repositorio nuevo llamado por ejemplo `miblog-copia`

y añadimos el repositorio remoto de Github a nuestro git local

```terminal
git remote add github https://github.com/USUARIO/miblog-copia.git
```


Ahora cada vez que queramos actualizar nuestra copia de Github, haremos `git push github master`.

## Flujo de trabajo para actualizar nuestro blog de Jekyll

1. Creamos/editamos una entrada nueva.
2. `git add . `
3. `git commit -m "un mensaje cualquiera"`
4. `git push droplet master` (para publicar la nueva entrada)
5. `git push github master` (para realizar copia de seguridad en Github)

Eso es todo. Un saludo navegantes!


[tutorial]: https://www.digitalocean.com/community/tutorials/how-to-deploy-jekyll-blogs-with-git
[comunidad-DO]: https://www.digitalocean.com/community
[git]: http://git-scm.com/
[homebrew]: http://brew.sh/
[jekyll]: http://jekyllrb.com/