---
title: Sobre mí
subtitle: Sin mucho que contar
displayInMenu: true
displayInList: false
---

¡Hola! Estás en una página personal como otras tantas que te puedes encontrar en internet. Pero lo realmente extraordinario, es que esta página es la mía. Con esto no quiero decir que por ser mía, es la mejor del mundo, lo que quiero decir es, que es única. No encontrarás una igual a esta. 

Este tinglao está alojado como página estática en [Gitlab](https://gitlab.com/tarteka/tarteka.gitlab.io) y creada con [Hugo](https://gohugo.io/). El tema de la página se llama [Aether](https://github.com/josephhutch/aether)
